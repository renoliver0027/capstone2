const express = require("express");
const UserController = require("../controllers/UserController.js");
const { verifyToken, verifyAdmin } = require("../auth.js");

const router = express.Router();

// User registration
router.post("/register", (request, response) => {
  UserController.registerUser(request, response);
});

// User authentication
router.post("/login", (request, response) => {
  UserController.loginUser(request, response);
});

// Retrieve User Details
router.post("/user", verifyToken, (request, response) => {
  UserController.detailsUser(request, response);
});

// Set user as admin (Admin only)
router.put("/:id/toAdmin", verifyToken, verifyAdmin, (request, response) => {
  UserController.setUserToAdmin(request, response);
});

router.put("/:id/toUser", verifyToken, verifyAdmin, (request, response) => {
  UserController.setAdminToUser(request, response);
});

router.get("/", verifyToken, verifyAdmin, (request, response) => {
  UserController.allUsers(request, response);
});

router.put("/:id", verifyToken, (request, response) => {
  UserController.editUserDetails(request, response);
});

module.exports = router;
