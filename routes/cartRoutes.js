const express = require("express");
const CartController = require("../controllers/CartController.js");
const { verifyToken } = require("../auth.js");

const router = express.Router();

router
  .route("/")
  // Create a cart (Added Products)
  .post(verifyToken, (request, response) => {
    CartController.createCart(request, response);
  });
// .get(verifyToken, (request, response) => {
//   CartController.getUserCart(request, response);
// });

router.get("/:userId", verifyToken, (request, response) => {
  CartController.getUserCart(request, response);
});

// To Add more products
router.post("/:cartId/add", verifyToken, (request, response) => {
  CartController.addToCart(request, response);
});

// Change product quantities (with Subtotal and Total)
router.put("/:cartId/edit", verifyToken, (request, response) => {
  CartController.editCart(request, response);
});

// Remove products from cart (with Subtotal and Total)
router.put("/:cartId/remove", verifyToken, (request, response) => {
  CartController.removeItems(request, response);
});

module.exports = router;
