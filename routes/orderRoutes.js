const express = require("express");
const OrderController = require("../controllers/OrderController.js");
const { verifyToken, verifyAdmin } = require("../auth.js");

const router = express.Router();

// Retrieve all orders (Admin only)
router.get("/all", verifyToken, verifyAdmin, (request, response) => {
  OrderController.getAllOrders(request, response);
});

// Non-admin User checkout (Create Order)
router.post("/checkout", verifyToken, (request, response) => {
  OrderController.createOrder(request, response);
});

// Retrieve authenticated user's orders
router.get("/:userId", verifyToken, (request, response) => {
  OrderController.getUsersOrders(request, response);
});

module.exports = router;
