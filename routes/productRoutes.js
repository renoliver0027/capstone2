const express = require("express");
const ProductController = require("../controllers/ProductController.js");
const { verifyToken, verifyAdmin } = require("../auth.js");

const router = express.Router();

// Create Product (Admin only)
router.post("/create", verifyToken, verifyAdmin, (request, response) => {
  ProductController.createProduct(request, response);
});

// Retrieve all products
router.get("/", (request, response) => {
  ProductController.getAllProducts(request, response);
});

// Retrieve all active products
router.get("/active", (request, response) => {
  ProductController.getAllActiveProducts(request, response);
});

router
  .route("/:id")
  // Retrieve single product
  .get(verifyToken, (request, response) => {
    ProductController.getSingleProduct(request, response);
  })
  // Update Product information (Admin only)
  .put(verifyToken, verifyAdmin, (request, response) => {
    ProductController.updateSingleProduct(request, response);
  });

// Archive Product (Admin only)
router.put("/:id/archive", verifyToken, verifyAdmin, (request, response) => {
  ProductController.archiveProduct(request, response);
});

// Activate Product (Admin only)
router.put("/:id/activate", verifyToken, verifyAdmin, (request, response) => {
  ProductController.activateProduct(request, response);
});

module.exports = router;
