require("dotenv").config();
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const cartRoutes = require("./routes/cartRoutes.js");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ extended: true, limit: "50mb" }));
app.use(cors());

app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);
app.use("/api/cart", cartRoutes);

mongoose.connect(
  `mongodb+srv://admin:${process.env.MONGO_PW}@cluster0.cj5nnq8.mongodb.net/capstone2-ecommerce-api?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.on("error", () => console.log("Can't connect to database"));
mongoose.connection.once("open", () => {
  console.log("Connected to MongoDB!"),
    app.listen(PORT, () =>
      console.log(`E-Commerce API is now running at localhost: ${PORT}`)
    );
});

module.exports = app;
