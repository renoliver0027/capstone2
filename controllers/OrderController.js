const Order = require("../models/Order.js");
const Cart = require("../models/Cart.js");

// Retrieve all orders (Admin only)
module.exports.getAllOrders = (request, response) => {
  return Order.find({})
    .then((orders) => {
      if (!orders) {
        return response.status(404).send("No orders yet.");
      }
      return response.status(200).send(orders);
    })
    .catch((error) => response.status(500).send(error.message));
};

// Non-admin User checkout (Create Order)
module.exports.createOrder = async (request, response) => {
  const { cartId, userId } = request.body;

  let cartItems = await Cart.findById(cartId).then((cart) => {
    if (!cart) {
      return response.status(404).send(`No cart found with id: ${cartId}`);
    }
    return cart;
  });

  const { products, totalAmount } = cartItems;

  let new_order = new Order({
    userId,
    products,
    totalAmount,
  });

  await new_order.save();

  return Cart.findByIdAndDelete(cartId)
    .then((orders, error) => {
      if (error) {
        return response.status(400).send(error.message);
      }
      return response.status(201).send({
        message: "Successfully ordered items from your cart :)",
        items: new_order,
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

// Retrieve authenticated user's orders
module.exports.getUsersOrders = (request, response) => {
  return Order.findOne({ userId: request.params.userId })
    .then((orders) => {
      if (!orders) {
        return response.status(404).send("No orders yet.");
      }
      return response.status(200).send(orders);
    })
    .catch((error) => response.status(500).send(error.message));
};
