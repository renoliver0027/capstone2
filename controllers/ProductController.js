const Product = require("../models/Product.js");
const cloudinary = require("cloudinary").v2;

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

// Create Product (Admin only)
module.exports.createProduct = async (request, response) => {
  const { name, description, price, image, isActive, createdOn } = request.body;

  const imageUrl = await cloudinary.uploader.upload(image, {
    upload_preset: "capstone3",
    allowed_formats: ["png", "jpg", "jpeg", "svg", "ico", "jfif", "webp"],
  });
  // .then((error, result) => {
  //   if (error) {
  //     console.log(error);
  //   }
  //   console.log(result);
  // });

  if (!name || !description || !price) {
    return response
      .status(400)
      .send("Cannot register product! Missing name/description/price.");
  }

  return Product.findOne({ name }).then((product) => {
    if (product) {
      return response.status(409).send("Product already registered!");
    }

    let new_product = new Product({
      name,
      description,
      price,
      isActive,
      image: imageUrl.secure_url,
      createdOn,
    });

    return new_product
      .save()
      .then((product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(201).send({
          message: "Product has been successfully registered!",
          product: product,
        });
      })
      .catch((error) => response.status(500).send(error.message));
  });
};

// Retrieve all products
module.exports.getAllProducts = (request, response) => {
  return Product.find({})
    .then((products) => {
      if (!products) {
        return response.status(404).send("No products found :(");
      }

      return response.status(200).send(products);
    })
    .catch((error) => response.status(500).send(error.message));
};

// Retrieve all active products
module.exports.getAllActiveProducts = (request, response) => {
  return Product.find({ isActive: true })
    .then((products) => {
      if (!products) {
        return response.status(404).send("No products found :(");
      }

      return response.status(200).send(products);
    })
    .catch((error) => response.status(500).send(error.message));
};

// Retrieve single product
module.exports.getSingleProduct = (request, response) => {
  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send(`No product found with id: ${request.params.id}`);
      }
      return response.status(200).send(product);
    })
    .catch((error) => response.status(500).send(error.message));
};

// Update Product information (Admin only)
module.exports.updateSingleProduct = (request, response) => {
  const { name, description, price } = request.body;

  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send(`No product found with id: ${request.params.id}`);
      }

      product.name = name;
      product.description = description;
      product.price = price;

      return product.save().then((updated_product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(updated_product);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

// Archive Product (Admin only)
module.exports.archiveProduct = (request, response) => {
  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send(`No product found with id: ${request.params.id}`);
      }

      product.isActive = false;

      return product.save().then((archived_product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(archived_product);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

// Activate Product (Admin only)
module.exports.activateProduct = (request, response) => {
  return Product.findById(request.params.id)
    .then((product) => {
      if (!product) {
        return response
          .status(404)
          .send(`No product found with id: ${request.params.id}`);
      }

      product.isActive = true;

      return product.save().then((archived_product, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(archived_product);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};
