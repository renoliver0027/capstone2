const Cart = require("../models/Cart.js");

/*
  Note: All functions have (Subtotal for each item) and (Total price for all items)
*/

// Create a cart (Added Products)
module.exports.createCart = async (request, response) => {
  const { userId, productId, productPrice, quantity } = request.body;

  return await Cart.findOne({ userId })
    .then((user) => {
      if (user) {
        return response
          .status(400)
          .send(
            "You already have a cart, try to add items in that cart instead."
          );
      } else {
        let new_items = new Cart({
          userId,
          products: [
            { productId, quantity, subtotal: productPrice * quantity },
          ],
          totalAmount: productPrice * quantity,
        });

        return new_items.save().then((cart_items, error) => {
          if (error) {
            return response.status(400).send(error.message);
          }
          return response.status(201).send(cart_items);
        });
      }
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.getUserCart = (request, response) => {
  return Cart.findOne({ userId: request.params.userId })
    .then((cart) => {
      if (!cart) {
        return response.status(404).send("Your cart is empty.");
      }
      return response.status(200).send(cart);
    })
    .catch((error) => response.status(500).send(error.message));
};

// To Add more products
module.exports.addToCart = async (request, response) => {
  const { userId, productId, productPrice, quantity } = request.body;

  return await Cart.findById(request.params.cartId)
    .then((cart) => {
      if (!cart) {
        return response
          .status(404)
          .send(`No cart found with id: ${request.params.cartId}`);
      } else {
        if (cart.userId.toString() !== userId) {
          return response
            .status(403)
            .send("You don't have permission to access this page.");
        }

        let add_new_items = {
          productId,
          quantity,
          subtotal: productPrice * quantity,
        };

        cart.products.push(add_new_items);
        cart.totalAmount += productPrice * quantity;

        return cart.save().then((new_products, error) => {
          if (error) {
            return response.status(400).send(error.message);
          }
          return response.status(201).send(new_products);
        });
      }
    })
    .catch((error) => response.status(500).send(error.message));
};

// Change products quantities
module.exports.editCart = async (request, response) => {
  const { productId, productPrice, quantity } = request.body;

  return await Cart.findById(request.params.cartId)
    .then(async (cart) => {
      if (!cart) {
        return response
          .status(404)
          .send(`No cart found with id: ${request.params.cartId}`);
      }

      if (cart.userId.toString() !== request.user.id) {
        return response
          .status(403)
          .send("You don't have permission to access this page.");
      }

      cart.totalAmount = 0;

      if (cart.products.length >= 1) {
        for (i = 0; i < cart.products.length; i++) {
          if (productId === cart.products[i].productId) {
            cart.products[i].quantity = quantity;
            cart.products[i].subtotal = productPrice * quantity;

            await cart.save();
          }

          cart.totalAmount += cart.products[i].subtotal;
        }
      }

      return cart.save().then((updated_item, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(updated_item);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

// Remove products from cart
module.exports.removeItems = async (request, response) => {
  const { products_id } = request.body;

  return await Cart.findById(request.params.cartId)
    .then(async (cart) => {
      if (!cart) {
        return response
          .status(404)
          .send(`No cart found with id: ${request.params.cartId}`);
      }

      if (cart.userId.toString() !== request.user.id) {
        return response
          .status(403)
          .send("You don't have permission to remove this item/s.");
      }

      cart.products.pull(products_id);

      await cart.save();

      cart.totalAmount = 0;

      if (cart.products.length >= 1) {
        for (i = 0; i < cart.products.length; i++) {
          cart.totalAmount += cart.products[i].subtotal;
        }
      }

      return cart.save().then((item, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send({
          message: "You have successfully removed the item/s.",
          remaining_items: item,
        });
      });
    })
    .catch((error) => response.status(500).send(error.message));
};
