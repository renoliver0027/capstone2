const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User registration
module.exports.registerUser = (request, response) => {
  const { name, email, password, isAdmin } = request.body;

  if (!name || !email || !password) {
    return response
      .status(400)
      .send("Register failed! All fields are required.");
  }

  return User.findOne({ email })
    .then((user) => {
      if (user) {
        return response
          .status(409)
          .send("User already registered, you may login instead.");
      } else {
        let new_user = new User({
          name,
          email,
          password: bcrypt.hashSync(password, 10),
          isAdmin,
        });

        return new_user.save().then((registered_user, error) => {
          if (error) {
            return response.status(400).send({ message: error.message });
          }
          return response.status(201).send({
            message: "Successfully registered a user!",
            user: registered_user,
          });
        });
      }
    })
    .catch((error) => response.status(500).send(error.message));
};

// User authentication
module.exports.loginUser = (request, response) => {
  const { email, password } = request.body;

  if (!email || !password) {
    return response
      .status(400)
      .send("Login failed! Missing email or password.");
  }

  return User.findOne({ email })
    .then((user) => {
      if (!user) {
        return response.status(404).send("No user found! Please signup first.");
      }

      const isMatched = bcrypt.compareSync(password, user.password);

      if (isMatched) {
        return response.status(200).send({
          accessToken: auth.createAccessToken(user),
          userId: user._id,
          isAdmin: user.isAdmin,
          message: "Login successful!",
        });
      } else {
        return response.status(400).send("Login failed! Incorrect Password.");
      }
    })
    .catch((error) => response.status(500).send(error.message));
};

// Retrieve User Details
module.exports.detailsUser = (request, response) => {
  return User.findOne({ _id: request.body.id })

    .then((user) => {
      return response.status(200).send(user);
    })
    .catch((error) => response.status(500).send(error.message));
};

// Set user as admin (Admin only)
module.exports.setUserToAdmin = (request, response) => {
  return User.findOne({ _id: request.params.id })
    .select("-password")
    .then((user) => {
      if (!user) {
        return response
          .status(404)
          .send(`No user found with id: ${request.params.id}`);
      }

      user.isAdmin = true;

      return user.save().then((user_admin, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(user_admin);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.setAdminToUser = (request, response) => {
  return User.findOne({ _id: request.params.id })
    .select("-password")
    .then((user) => {
      if (!user) {
        return response
          .status(404)
          .send(`No user found with id: ${request.params.id}`);
      }

      user.isAdmin = false;

      return user.save().then((user_admin, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(user_admin);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};

module.exports.allUsers = (request, response) => {
  return User.find({})
    .select("-password")
    .then((users) => {
      if (!users) {
        return response.send("No registered users");
      }
      return response.send(users);
    })
    .catch((error) => response.send(error.message));
};

module.exports.editUserDetails = (request, response) => {
  const {
    name,
    email,
    // password
  } = request.body;

  return User.findOne({ _id: request.params.id })
    .then((user) => {
      user.name = name;
      user.email = email;
      // user.password = bcrypt.hashSync(password, 10);

      return user.save().then((updatedUser, error) => {
        if (error) {
          return response.status(400).send(error.message);
        }
        return response.status(200).send(updatedUser);
      });
    })
    .catch((error) => response.status(500).send(error.message));
};
