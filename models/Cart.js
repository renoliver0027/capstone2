const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: [true, "userId is required"],
  },
  products: [
    {
      productId: {
        type: String,
        required: [true, "productId is required"],
      },
      quantity: {
        type: Number,
        required: [true, "Quantity is required"],
      },
      subtotal: {
        type: Number,
      },
    },
  ],
  totalAmount: {
    type: Number,
  },
  addedOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Cart", cartSchema);
