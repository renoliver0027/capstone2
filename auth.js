const jwt = require("jsonwebtoken");
const secret_key = process.env.SECRET_KEY;

module.exports.createAccessToken = (user) => {
  const user_data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(user_data, secret_key, { expiresIn: "1d" });
};

module.exports.verifyToken = (request, response, next) => {
  let token = request.headers.authorization;

  if (typeof token === undefined || token === null) {
    return response.send({
      auth: "Access Denied! Please input token in the headers",
    });
  }

  token = token.slice(7, token.length);

  jwt.verify(token, secret_key, (error, decoded_token) => {
    if (error) {
      return response.status(401).send({
        auth: "Access Denied",
        message: error.message,
      });
    }

    request.user = decoded_token;

    next();
  });
};

module.exports.verifyAdmin = (request, response, next) => {
  if (!request.user.isAdmin) {
    return response.status(403).send({
      auth: "Access Denied",
      message: "Only Admins can access this site",
    });
  }

  next();
};
